# AngularInputOutput

Consists of 2 components, appComponent and stockStatusComponent. appComponents acts as the parent to stockStatusComponent.
Intended to demonstrate the use of @input and @output to communicate between parent and child components.

# Walkthrough

https://dzone.com/articles/understanding-output-and-eventemitter-in-angular