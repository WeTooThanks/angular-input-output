import { Component, Input, EventEmitter, Output, OnChanges } from '@angular/core';

@Component({
  selector: 'app-stock-status',
  templateUrl: './stock-status.component.html',
  styleUrls: ['./stock-status.component.css']
})
export class StockStatusComponent implements OnChanges {

  constructor() { }

  // Inputs denote data passed from the parent
  @Input() stock: number;
  @Input() productId: number;
  @Output() stockValueChange = new EventEmitter();
  color = '';
  updatedStockValue: number;   // bound to in the template using ngModel

  stockValueChanged(){
    this.stockValueChange.emit({ id: this.productId, updatedStockValue: this.updatedStockValue });
    this.updatedStockValue = null;
  }

  ngOnChanges() {
    if (this.stock > 10){
      this.color = 'green';
    }
    else{
      this.color = 'red';
    }
  }
}
